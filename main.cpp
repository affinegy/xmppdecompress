#include <iostream>
#include <fstream>
#include <zlib.h>
#include <stdint.h>
#include <stdlib.h>
#include <csignal>
#include <string.h>
#include <vector>
#include <sstream>

using namespace std;

static void SigIntHandler(int sig)
{
    cout << "Handling SIGINT" << endl;
    exit(sig);
}

static inline vector<string> &split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

static inline vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

uint8_t CharToU8(const char c)
{
    if (c >= '0' && c <= '9') {
        return (uint8_t)(c - '0');
    }
    if (c >= 'A' && c <= 'F') {
        return (uint8_t)(10 + c - 'A');
    }
    if (c >= 'a' && c <= 'f') {
        return (uint8_t)(10 + c - 'a');
    }
    return 255;
}

size_t HexStringToBytes(const string& hex, uint8_t* outBytes, size_t len, char separator = 0)
{
    if (separator) {
        len = min((1 + hex.length()) / 3, len);
    } else {
        len = min(hex.length() / 2, len);
    }
    string::const_iterator it = hex.begin();
    for (size_t i = 0; i < len; i++) {
        if (separator && (i != 0)) {
            if (*it++ != separator) {
                len = i;
                break;
            }
        }
        uint8_t h = CharToU8(*it++);
        uint8_t l = CharToU8(*it++);
        if ((h > 15) || (l > 15)) {
            len = i;
            break;
        }
        outBytes[i] = (uint8_t)((h << 4) + l);
    }
    return len;
}


string
Decompress(
    const string& str
)
{
        size_t numBytes = str.length()/2;
        uint8_t* bytes = new uint8_t[numBytes];
        if(numBytes != HexStringToBytes(str.c_str(), bytes, numBytes))
        {
            return str;
        }

        z_stream zs;
        memset(&zs, 0, sizeof(zs));

        if(inflateInit(&zs) != Z_OK)
        {
            return str;
        }

        zs.next_in = (Bytef*)bytes;
        zs.avail_in = numBytes;

        int ret;
        char outBuffer[32768];
        string outString;
        do
        {
            zs.next_out = reinterpret_cast<Bytef*>(outBuffer);
            zs.avail_out = sizeof(outBuffer);

            ret = inflate(&zs, 0);

            if(outString.size() < zs.total_out)
            {
                outString.append(outBuffer, zs.total_out-outString.size());
            }
        } while(ret == Z_OK);

        inflateEnd(&zs);

        if(ret != Z_STREAM_END)
        {
            return str;
        }

        return outString;
}


int main(int argc, char** argv)
{
    signal(SIGINT, SigIntHandler);
    bool fromInputStream(false);

    if ( argc < 2 )
    {
        cout << "Decompressing input stream..." << endl;
        cout.flush();
        fromInputStream = true;
    }

    istream* is = 0;
    if ( !fromInputStream ) {
        char* infile = argv[1];

        // Read in the configuration file
        ifstream* conf_file = new ifstream(infile);
        if ( !conf_file->is_open() )
        {
            cerr << "Could not open " << infile << "!" << endl;
            exit(1);
        }
        is = conf_file;
    } else {
        is = &std::cin;
    }
    string line;
    enum filetype {
        unknown,
        chat,
        log
    } type = unknown;
    while ( getline( *is, line ) )
    {
        if ( type == unknown )
        {
            if ( line.substr(0,2) == "0x" )
            {
                type = log;
            }
            else if ( line.substr(2,1) == ":" && line.substr(13,2) == "0x" )
            {
                type = log;
            }
            else
            {
                type = chat;
            }
        }
        if ( type == chat )
        {
            vector<string> tokens = split(line, ' ');
            if ( tokens.size() < 4 )
            {
                cout << endl;
                continue;
            }
            string compressed( tokens[3] );
            if ( compressed.empty() )
            {
                cout << endl;
                continue;
            }
            string sender( tokens[0] + " " + tokens[1] + " " + tokens[2] );
            cout << sender << endl;
            string decompressed( Decompress( compressed ) );
	    cout << decompressed << endl;
        }
        else if ( type == log )
        {
            // TODO: Messages spanning multiple line, e.g.
            // <body>__NAME_OWNER_CHANGED
            // :Y6Mp1292.5
            //
            // :Y6Mp1292.5
            // </body>
            if ( string::npos != line.find("<body>") )
            {
                size_t start = line.find("<body>");
                start = (start == string::npos) ? 0 : start + 6;
                size_t end = line.find("</body>", start);
                if (end == string::npos)
                {
                    end = line.length() ? line.length() - 1 : 0;
                }
                size_t length = end - start;

                string compressed = line.substr(start, length);
                string token1 = line.substr(0, start);
                string token2 = Decompress( compressed );
                string token3 = line.substr(end, line.length() - end);
                cout << token1 << token2 << token3 << endl;
            }
            else
            {
                cout << line << endl;
            }
        }
    }

    if ( !fromInputStream ) {
        delete is;
    }

    return 0;
}


## Synopsis

xmppdecompress is used to take XMPP messages whose bodies are compressed and decompress them into plaintext.

There are 2 types of files that this works with:

1. Chat transcripts of xmppconn conversations taken from the chat window in Pidgin. The output is taken from a chat window in Pidgin and copied into a file. That file is used as the first argument to this program.
2. xmppconn log files

## Building from Source

All that needs to be done is to pull the code and execute make from the root folder.

    git clone https://bitbucket.org/affinegy/xmppdecompress.git
    make

## Installation

TODO: Describe installation

## License

Copyright (c) 2015 Affinegy, Inc. All rights reserved.
All information contained herein is, and remains the property of
Affinegy, Inc. and its suppliers, if any. The intellectual and
technical concepts contained herein are proprietary to Affinegy,
Inc. and its suppliers and may be covered by U.S. and Foreign
Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction
of this material is strictly forbidden unless prior written
permission is obtained from Affinegy, Inc.
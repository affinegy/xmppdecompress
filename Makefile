
CXX := g++
CC  := gcc

CXXFLAGS = -Wall -pipe -std=c++98 -fno-rtti -fno-exceptions -Wno-long-long -Wno-deprecated -g 
LDFLAGS  =
LIBS     =
INCLUDES =
DEFINES  =

LIBS += -lstdc++ -lz 

.PHONY: default clean

default: xmppdecompress

clean:
	rm -f *.o xmppdecompress

all: xmppdecompress

xmppdecompress: main.o
	$(CXX) -o $@ main.o $(LDFLAGS) $(LIBS)

main.o: main.cpp
	$(CXX) -c $(CXXFLAGS) $(INCLUDES) -o $@ main.cpp

